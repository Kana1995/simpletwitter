package chapter6.utils;

import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.dbunit.Assertion;
import org.dbunit.database.DatabaseConnection;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.database.QueryDataSet;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.csv.CsvDataSet;
import org.dbunit.dataset.csv.CsvDataSetWriter;
import org.dbunit.operation.DatabaseOperation;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import chapter6.beans.Message;
import chapter6.beans.User;
import junit.framework.TestCase;
	/**
	 * @author kana.nakajima
	 *
	 */
	public class DBUtilTest extends TestCase {

		private static final String DRIVER = "com.mysql.jdbc.Driver";
		private static final String URL = "jdbc:mysql://localhost/simple_twitter";
		private static final String USER = "root";
		private static final String PASSWORD = "root";

		public DBUtilTest(String name) {
			super(name);
		}

		private File file;

		//DB接続部分(DBUtil)
		static {
			try {
				Class.forName(DRIVER);
			} catch (ClassNotFoundException e) {
				throw new RuntimeException(e);
			}
		}

		private static Connection getConnection() throws Exception {
			Class.forName(DRIVER);
			Connection connection = DriverManager.getConnection(URL, USER, PASSWORD);
			return connection;
		}

		/**
		 * @throws java.lang.Exception
		 */
		@Before
		public void setUp() throws Exception {

			//(1)IDatabaseConnectionを取得
			IDatabaseConnection connection = null;
			try {
				super.setUp();
				Connection conn = getConnection();
				connection = new DatabaseConnection(conn);

				//(2)現状のバックアップを取得(テーブルごとに取得)
				QueryDataSet partialDataSet = new QueryDataSet(connection);
				partialDataSet.addTable("messages");
				partialDataSet.addTable("users");

				//バックアップをcsvで出力
				CsvDataSetWriter.write(partialDataSet,new File("backup"));

				//csv用データ投入
				IDataSet dataSetMessage = new CsvDataSet(new File("dataset"));
				DatabaseOperation.CLEAN_INSERT.execute(connection, dataSetMessage);

//				file = File.createTempFile("messages", ".xml");
//				file = File.createTempFile("users", ".xml");
//				FlatXmlDataSet.write(partialDataSet,new FileOutputStream(file));

//				//(3)テストデータを投入する
//				IDataSet dataSetMessage = new FlatXmlDataSet(new File("message_test_data.xml"));
//				DatabaseOperation.CLEAN_INSERT.execute(connection, dataSetMessage);



			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				try {
					if (connection != null)
						connection.close();
				} catch (SQLException e) {
				}
			}
		}

		/**
		 * テスト後の片付け
		 */
		@After
		public void tearDown() throws Exception {

			IDatabaseConnection connection = null;
			try {
				super.tearDown();
				Connection conn = getConnection();
				connection = new DatabaseConnection(conn);

				IDataSet dataSet = new CsvDataSet(new File("dataset"));
				DatabaseOperation.CLEAN_INSERT.execute(connection, dataSet);

			} catch (Exception e) {
				e.printStackTrace();
			} finally {

				//一時ファイルの削除
				if (file != null) {
					file.delete();
				}
				try {
					if (connection != null)
						connection.close();
				} catch (SQLException e) {
				}

			}

		}

		/**
		 * 参照メソッドのテスト
		 */
		@Test
		public void testSelectMessages() throws Exception {


			//参照メソッドの実行
			List<Message> resultList = DBUtil.allMessage();

			//値の検証

			//件数
			assertEquals(3, resultList.size());

			//データ
			Message result001 = resultList.get(0);
			assertEquals("id=1", "id=" + result001.getId());
			assertEquals("userId=1", "userId=" + result001.getUserId());
			assertEquals("text=ラーメン", "text=" + result001.getText());

			Message result002 = resultList.get(1);
			assertEquals("id=2", "id=" + result002.getId());
			assertEquals("userId=2", "userId=" + result002.getUserId());
			assertEquals("text=寿司", "text=" + result002.getText());

			Message result003 = resultList.get(2);
			assertEquals("id=3", "id=" + result003.getId());
			assertEquals("userId=3", "userId=" + result003.getUserId());
			assertEquals("text=カレー", "text=" + result003.getText());
		}


		@Test
		public void testSelectUsers() throws Exception {


			//参照メソッドの実行
			List<User> resultList = DBUtil.allUser();

			//値の検証

			//件数
			assertEquals(3, resultList.size());

			//データ
			User result001 = resultList.get(0);
			assertEquals("id=1", "id=" + result001.getId());
			assertEquals("account=アカウント１", "account=" + result001.getAccount());
			assertEquals("name=名前１", "name=" + result001.getName());
			assertEquals("email=@docomo", "email=" + result001.getEmail());
			assertEquals("password=パスワード１", "password=" + result001.getPassword());

			User result002 = resultList.get(1);
			assertEquals("id=2", "id=" + result002.getId());
			assertEquals("account=アカウント２", "account=" + result002.getAccount());
			assertEquals("name=名前２", "name=" + result002.getName());
			assertEquals("email=@docomo", "email=" + result002.getEmail());
			assertEquals("password=パスワード２", "password=" + result002.getPassword());

			User result003 = resultList.get(2);
			assertEquals("id=3", "id=" + result003.getId());
			assertEquals("account=アカウント３", "account=" + result003.getAccount());
			assertEquals("name=名前３", "name=" + result003.getName());
			assertEquals("email=@docomo", "email=" + result003.getEmail());
			assertEquals("password=パスワード３", "password=" + result003.getPassword());
		}

		/**
		 * 更新メソッドのテスト
		 */
		@Test
		public void testUpdateMessage() throws Exception {

			//テスト対象となる、storeメソッドを実行
			//テストのインスタンスを生成
			Message message001 = new Message();
			message001.setId(1);
			message001.setUserId(1);
			message001.setText("ラーメン");

			Message message002 = new Message();
			message002.setId(2);
			message002.setUserId(2);
			message002.setText("寿司");

			Message message003 = new Message();
			message003.setId(3);
			message003.setUserId(3);
			message003.setText("カレー");


			List<Message> messageSetList = new ArrayList<Message>();
			messageSetList.add(message001);
			messageSetList.add(message002);
			messageSetList.add(message003);

			DBUtil.updataMessage(messageSetList);

			//テスト結果として期待されるべきテーブルデータを表すITableインスタンスを取得
			IDatabaseConnection connection = null;
			try {

				Connection conn = getConnection();
				connection = new DatabaseConnection(conn);

				//メソッド実行した実際のテーブル
				IDataSet databaseDataSet = connection.createDataSet();
				ITable actualTableMessage = databaseDataSet.getTable("messages");


				// テスト結果として期待されるべきテーブルデータを表すITableインスタンスを取得
				IDataSet expectedDataSet = new CsvDataSet(new File("dataset"));
				ITable expectedTableMessage = expectedDataSet.getTable("messages");


//				// テスト結果として期待されるべきテーブルデータを表すITableインスタンスを取得
//				IDataSet expectedDataSet = new FlatXmlDataSet(new File("branch_test_data2.xml"));
//				ITable expectedTable = expectedDataSet.getTable("branch_sale_out");

				//期待されるITableと実際のITableの比較
				Assertion.assertEquals(expectedTableMessage, actualTableMessage);

			} finally {
				if (connection != null)
					connection.close();
			}

		}

		@Test
		public void testUpdateUser() throws Exception {

			//テスト対象となる、storeメソッドを実行
			//テストのインスタンスを生成
			User user001 = new User();
			user001.setId(1);
			user001.setAccount("アカウント１");
			user001.setName("名前１");
			user001.setEmail("@docomo");
			user001.setPassword("パスワード１");

			User user002 = new User();
			user002.setId(2);
			user002.setAccount("アカウント２");
			user002.setName("名前２");
			user002.setEmail("@docomo");
			user002.setPassword("パスワード２");

			User user003 = new User();
			user003.setId(3);
			user003.setAccount("アカウント３");
			user003.setName("名前３");
			user003.setEmail("@docomo");
			user003.setPassword("パスワード３");


			List<User> userSetList = new ArrayList<User>();
			userSetList.add(user001);
			userSetList.add(user002);
			userSetList.add(user003);

			DBUtil.updataUser(userSetList);

			//テスト結果として期待されるべきテーブルデータを表すITableインスタンスを取得
			IDatabaseConnection connection = null;
			try {

				Connection conn = getConnection();
				connection = new DatabaseConnection(conn);

				//メソッド実行した実際のテーブル
				IDataSet databaseDataSet = connection.createDataSet();
				ITable actualTableUser = databaseDataSet.getTable("users");

				// テスト結果として期待されるべきテーブルデータを表すITableインスタンスを取得
				IDataSet expectedDataSet = new CsvDataSet(new File("dataset"));
				ITable expectedTableUser = expectedDataSet.getTable("users");

//				// テスト結果として期待されるべきテーブルデータを表すITableインスタンスを取得
//				IDataSet expectedDataSet = new FlatXmlDataSet(new File("branch_test_data2.xml"));
//				ITable expectedTable = expectedDataSet.getTable("branch_sale_out");

				//期待されるITableと実際のITableの比較
				Assertion.assertEquals(expectedTableUser, actualTableUser);
			} finally {
				if (connection != null)
					connection.close();
			}

		}

	}






