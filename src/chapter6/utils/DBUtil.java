package chapter6.utils;

import java.io.FileOutputStream;
import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.dbunit.database.DatabaseConnection;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.dataset.xml.FlatDtdDataSet;

import chapter6.beans.Message;
import chapter6.beans.User;
import chapter6.exception.SQLRuntimeException;

/**
 * DB(コネクション関係)のユーティリティー
 */
public class DBUtil {

    private static final String DRIVER = "com.mysql.jdbc.Driver";
    private static final String URL = "jdbc:mysql://localhost/simple_twitter";
    private static final String USER = "root";
    private static final String PASSWORD = "root";

  //DB接続部分(DBUtil)
  	static {
  		try {
  			Class.forName(DRIVER);
  		} catch (ClassNotFoundException e) {
  			throw new RuntimeException(e);
  		}
  	}

//  	private static Connection getConnection() throws Exception {
//  		Class.forName(DRIVER);
//  		Connection connection = DriverManager.getConnection(URL, USER, PASSWORD);
//  		return connection;
//  	}

  	public static void main(String[] args) throws Exception {

  		// データベースに接続する。
  		IDatabaseConnection connection = new DatabaseConnection(getConnection());

  		// Dtdファイルを作成する
  		FlatDtdDataSet.write(connection.createDataSet(),
  				new FileOutputStream("test.dtd"));
  	}

  	//◇◇DBUnit用追加機能①Message参照メソッド
  	public static List<Message> allMessage() throws Exception {

  		Connection connection = null;
  		List<Message> branchesList = new ArrayList<Message>();

  		try {
  			connection = getConnection();
  			String sql = "SELECT * FROM messages ORDER BY id";
  			PreparedStatement ps = connection.prepareStatement(sql);

  			ResultSet result = ps.executeQuery();

  			while (result.next()) {
  				Message message = new Message();
  				message.setId(result.getInt("id"));
  				message.setUserId(result.getInt("user_id"));
  				message.setText(result.getString("text"));
  				message.setCreatedDate(result.getDate("created_date"));
  				message.setUpdatedDate(result.getDate("updated_date"));

  				branchesList.add(message);
  			}

  		} finally {
  			if (connection != null)
  				connection.close();
  		}
  		return branchesList;

  	}

  //◇◇DBUnit用追加機能①User参照メソッド
  	public static List<User> allUser() throws Exception {

  		Connection connection = null;
  		List<User> branchesList = new ArrayList<User>();

  		try {
  			connection = getConnection();
  			String sql = "SELECT * FROM users ORDER BY id";
  			PreparedStatement ps = connection.prepareStatement(sql);

  			ResultSet result = ps.executeQuery();

  			while (result.next()) {
  				User user = new User();
  				user.setId(result.getInt("id"));
  				user.setAccount(result.getString("account"));
  				user.setName(result.getString("name"));
  				user.setEmail(result.getString("email"));
  				user.setPassword(result.getString("password"));
  				user.setDescription(result.getString("description"));
  				user.setCreatedDate(result.getDate("created_date"));
  				user.setUpdatedDate(result.getDate("updated_date"));

  				branchesList.add(user);
  			}

  		} finally {
  			if (connection != null)
  				connection.close();
  		}
  		return branchesList;

  	}

  	//◇◇DBUnit用追加機能②Message更新メソッド
  	public static boolean updataMessage(List<Message> resultMessageList) throws Exception {

  		Connection connection = null;

  		for (int i = 0; i < resultMessageList.size(); i++) {

  			Message message = resultMessageList.get(i);

  			try {
  				connection = getConnection();
  				StringBuilder sql = new StringBuilder();
  				sql.append("REPLACE messages SET id = ?, user_id = ?,text = ?, created_date = ?, updated_date = ? ;");

  				PreparedStatement ps = connection.prepareStatement(sql.toString());

  				ps.setInt(1, message.getId());
  				ps.setInt(2, message.getUserId());
  				ps.setString(3, message.getText());
  				ps.setDate(4, (Date) message.getCreatedDate());
  				ps.setDate(5, (Date) message.getUpdatedDate());

  				ps.executeUpdate();

  			} finally {
  				try {
  					if (connection != null) {
  						connection.close();
  					}
  				} catch (SQLException e) {
  					return false;
  				}
  			}
  		}
  		return true;

  	}

  //◇◇DBUnit用追加機能②User更新メソッド
  	public static boolean updataUser(List<User> resultUserList) throws Exception {

  		Connection connection = null;

  		for (int i = 0; i < resultUserList.size(); i++) {

  			User user = resultUserList.get(i);

  			try {
  				connection = getConnection();
  				StringBuilder sql = new StringBuilder();
  				sql.append("REPLACE users SET id = ?, account = ?, name = ?, "
  						+ "email = ?, password = ?, description = ?, created_date = ?, updated_date = ? ;");

  				PreparedStatement ps = connection.prepareStatement(sql.toString());

  				ps.setInt(1, user.getId());
  				ps.setString(2, user.getAccount());
  				ps.setString(3, user.getName());
  				ps.setString(4, user.getEmail());
  				ps.setString(5, user.getPassword());
  				ps.setString(6, user.getDescription());
  				ps.setDate(7, (Date) user.getCreatedDate());
  				ps.setDate(8, (Date) user.getUpdatedDate());

  				ps.executeUpdate();

  			} finally {
  				try {
  					if (connection != null) {
  						connection.close();
  					}
  				} catch (SQLException e) {
  					return false;
  				}
  			}
  		}
  		return true;

  	}

    /**
     * コネクションを取得します。
     *
     * @return
     */
    public static Connection getConnection() {

        try {
        	//コネクションの取得とオートコミットモードの変更
            Connection connection = DriverManager.getConnection(URL, USER, PASSWORD);
            connection.setAutoCommit(false);
            return connection;
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        }
    }


    /**
     * コミットします。
     *
     * @param connection
     */
    public static void commit(Connection connection) {

        try {
            connection.commit();
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        }
    }

    /**
     * ロールバックします。
     *
     * @param connection
     */
    public static void rollback(Connection connection) {

        if (connection == null) {
            return;
        }

        try {
            connection.rollback();
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        }
    }
}
